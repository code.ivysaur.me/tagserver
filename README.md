# tagserver

![](https://img.shields.io/badge/written%20in-golang-blue)

An in-memory key-value store with HTTP interface.

This is a quick-and-dirty key-value store accessible over HTTP.

- Both keys and values must be representable as a string.
- Keys are split into two parts in order to optimise first-level list queries.
- The underlying structure allows concurrent read queries, but a write will temporarily lock the storage against reads.
- The data is asynchronously persisted to disk; after every write, a timestamp is sent to a background writer thread. If the writer's last write started before this timestamp, a new write action will be initiated.
- Binaries are provided for win64, but the code is cross-platform Golang and should compile cleanly on other platforms.

## Daemon


```
Usage of tagserver:
  -bindaddr string
        Bind address, can be blank
  -datafile string
        Persistence storage file (default "tagserver.json")
  -debug
        Display debug log on stdout
  -port int
        Port for web server (default 8080)

```


## API

- GET `/get/KEY_FIRST/KEY_SECOND`
- GET `/get/KEY_FIRST`
- POST `/set/KEY_FIRST/KEY_SECOND`

## Example Usage


```
$ curl -X POST -d "thevalue" http://HOST/set/KEY_FIRST/KEY_SECOND
OK
$ curl http://HOST/get/KEY_FIRST/KEY_SECOND
thevalue
$ curl http://HOST/get/KEY_FIRST/
{"KEY_SECOND":"thevalue"}
$ curl http://HOST/get/KEY_FIRST/KEY_NOT_YET_SET
404 Not Found

```


## Changelog

2015-10-08: r04
- Initial public release
- [⬇️ tagserver-r04.7z](dist-archive/tagserver-r04.7z) *(1.43 MiB)*

